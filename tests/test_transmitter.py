import logging
import random
import sys
import skyscreen_core.input
import os
import time

logging.basicConfig(level=logging.DEBUG)

pid = os.fork()
port = 5556

if not pid:
	# The child is the receiver
	recv = skyscreen_core.input.ZMQPatternParamsRecv(port)
	with recv:
		while True:
			print(recv.p1, recv.p2, recv.p3, recv.p4)
			time.sleep(0.1)
			recv.read_params()
else:
	trans = skyscreen_core.input.ZMQPatternParamsSend(port)
	with trans:
		while True:
			trans.send_params(
				random.randint(0, 10),
				random.randint(0, 10),
				random.randint(0, 10),
				random.randint(0, 10))
			time.sleep(0.1)
